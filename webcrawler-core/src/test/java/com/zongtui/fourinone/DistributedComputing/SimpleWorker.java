package com.zongtui.fourinone.DistributedComputing;


import com.zongtui.fourinone.file.WareHouse;
import com.zongtui.fourinone.worker.MigrantWorker;

public class SimpleWorker extends MigrantWorker
{	
	public WareHouse doTask(WareHouse inhouse)
	{
		String word = inhouse.getString("word");
		System.out.println(word+" from Contractor.");
		 //single download
//        Spider spider = Spider.create(new BaiduBaikePageProcessor()).thread(2);
//        String urlTemplate = "http://baike.baidu.com/search/word?word=%s&pic=1&sug=1&enc=utf8";
//        ResultItems resultItems = spider.<ResultItems>get(String.format(urlTemplate, "水力发电"));
//        System.out.println(resultItems);

        //multidownload
//        List<String> list = new ArrayList<String>();
//        list.add(String.format(urlTemplate,"风力发电"));
//        list.add(String.format(urlTemplate,"太阳能"));
//        list.add(String.format(urlTemplate,"地热发电"));
//        list.add(String.format(urlTemplate,"地热发电"));
//        List<ResultItems> resultItemses = spider.<ResultItems>getAll(list);
//        for (ResultItems resultItemse : resultItemses) {
//            System.out.println(resultItemse.getAll());
//        }
//        spider.close();
		return new WareHouse("word", word+" world!");
	}
	
	public static void main(String[] args)
	{
		SimpleWorker mw = new SimpleWorker();
		mw.waitWorking("simpleworker");
	}
}