package com.zongtui.fourinone.park;

import com.zongtui.fourinone.exception.ClosetoOverException;
import com.zongtui.fourinone.exception.LeaderException;
import com.zongtui.fourinone.obj.ObjValue;

import java.rmi.RemoteException;

public interface Park extends ParkActive {

    public ObjValue create(String domain, String node, byte[] bts, String sessionid, int auth, boolean heartbeat) throws RemoteException, ClosetoOverException;//acl

    public ObjValue update(String domain, String node, byte[] bts, String sessionid) throws RemoteException, ClosetoOverException;//acl

    public boolean update(String domain, int auth, String sessionid) throws RemoteException;

    public ObjValue get(String domain, String node, String sessionid) throws RemoteException, ClosetoOverException;

    public ObjValue getLastest(String domain, String node, String sessionid, long version) throws RemoteException, ClosetoOverException;

    public ObjValue delete(String domain, String node, String sessionid) throws RemoteException, ClosetoOverException;

    public String getSessionId() throws RemoteException;

    public boolean heartbeat(String[] domainnodekey, String sessionid) throws RemoteException;

    public ObjValue getParkinfo() throws RemoteException;

    public boolean setParkinfo(ObjValue ov) throws RemoteException;

    public String[] askMaster() throws RemoteException;

    public boolean askLeader() throws RemoteException, LeaderException;
}