package com.zongtui.fourinone.park;

import java.rmi.Remote;

/**
 * 远程调用基础对象，全局使用.【活动对象】
 */
public interface ParkActive extends ParkStatg, Remote {
}